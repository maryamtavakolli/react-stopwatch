import React from 'react';
import { ClockPage,StopWatchPage } from "./_pages/index";
import { BrowserRouter as Router, Route} from "react-router-dom";
import CountDown from "./_pages/timer.page";
import './App.css';



function App (){
    return (
      <Router>
         <Route path="/" exact component={ClockPage}/>
         <Route path="/stopwatch" component={StopWatchPage}/>
        <Route path="/Timer" component={CountDown}/>
      {/* <Redirect to="/" /> */}
      </Router>
      // <div className="App">
      //   <div className="App-title">Timers Demo</div>
      //   <div className="Timers">
     
      //     <Countdown />
      //   </div>
      // </div>
    );
    
   
}

export default App;
